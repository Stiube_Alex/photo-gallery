<?php
require('db.php');
session_start();
// When form submitted, insert values into the database.
if (isset($_REQUEST['username'])) {
    // removes backslashes
    $username = stripslashes($_REQUEST['username']);
    $email    = stripslashes($_REQUEST['email']);
    $password = stripslashes($_REQUEST['password']);
    $confirmPassword = stripslashes($_REQUEST['confirmPassword']);
    //escapes special characters in a string
    $username = mysqli_real_escape_string($con, $username);
    $email    = mysqli_real_escape_string($con, $email);
    $password = mysqli_real_escape_string($con, $password);
    $confirmPassword = mysqli_real_escape_string($con, $confirmPassword);
    $create_datetime = date("Y-m-d H:i:s");
    $query = "INSERT INTO `users` (username, password, email, create_datetime)
              VALUES ('$username', '" . md5($password) . "', '$email', '$create_datetime')";
    if ($password == $confirmPassword) {
        $result = mysqli_query($con, $query);
        if ($result) {
            $query = "SELECT * FROM users";
            $result = mysqli_query($con, $query);
            while ($row = mysqli_fetch_assoc($result)) {
                $userId = $row['id'];
            }
            $query = "INSERT INTO profile_img (user_id, status) VALUES('$userId',0)";
            mysqli_query($con, $query) or die("Error");
            echo "<div class='form'>
                      <h3>You are registered successfully.</h3><br/>
                      <p class='link'>Click here to <a href='homeLogin.php'>Login</a></p>
                      </div>";
        } else {
            echo "<div class='form'>
                      <h3>Something went wrong!</h3><br/>
                      <p class='link'>Click here to <a href='homeRegistration.php'>register</a> again.</p>
                      </div>";
        }
    } else {
        echo "<div class='form'>
        <h3>Passwords do not match!</h3><br/>
        <p class='link'>Click here to <a href='homeRegistration.php'>register</a> again.</p>
        </div>";
    }
} else {
    include "templates/registrationForm.html";
}
