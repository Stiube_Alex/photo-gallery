<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Photo Gallery</title>
  <link rel="stylesheet" href="style/mainPage.css">
  <?php include "templates/iconInsertion.html"; ?>
</head>

<body>
  <?php
  require 'components/db.php';
  include 'templates/navbarMain.html';
  ?>
  <div class="overlay"></div>
  <div class="userListDiv mb-4">
    <h2 class="userListTitle m-0">Users List</h2>
  </div>

  <?php
  $query = "SELECT * FROM users ORDER BY create_datetime";
  $result = mysqli_query($con, $query);
  if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
      $id_user = $row['id'];
      $username = $row['username'];
      $email = $row['email'];
      $created_at = $row['create_datetime'];
      $isAdmin = $row['admin'];

      echo   "<div class='userDiv rounded mb-4'>
    <img class='userPic' src='assets/default user.png' alt=''> " .
        "<w class='mx-3';>Username: " . $username . " Email: " . $email . " Joined: " . $created_at . "</w>
    <button class='del right btn btn-outline-danger'>Delete</button>
    <button class='yn no right btn btn-outline-success'>No</button>
    <button class='yn yes_ right btn btn-outline-danger' data-iduser='$id_user'>Yes</button>
  </div>";
    }
  }
  ?>


  <?php
  include "templates/footer.html";
  include "templates/bootstrapInsertion.html";
  ?>

</body>

</html>
<style>
  .userListDiv {
    text-align: center;
    width: 30%;
    margin: 0 auto;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.75);
    background-color: white;
    border: 1px solid black;
    border-radius: 4px
  }

  .userListTitle {
    padding: 8px;
  }

  .userDiv {
    border: 1px solid black;
    background-color: white;
    display: block;
    margin: 0 auto;
    width: 80%;
    padding: 20px;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.75);
  }

  .userPic {
    height: 10px;
    width: auto;
    transform: scale(3, 3);
    border-radius: 1px;
    margin-bottom: 6.5px;
  }

  .right {
    float: right;
    transform: translateY(-8px);
  }

  .yn {
    display: none;
    margin: 0 5px;
  }
</style>
<script>
  $('.del').click(function() {
    $(this).hide();
    $(this).next().show();
    $(this).next().next().show();
  })
  $('.no').click(function() {
    $(this).prev().show();
    $(this).hide();
    $(this).next().hide();
  })
  $('.yes_').click(function() {
    // console.log($(this)[0].dataset.iduser);
    $.post('components/deleteUser.php', {
      id_user: $(this)[0].dataset.iduser
    }).done(function() {
      window.location.reload();
    })
  })
</script>