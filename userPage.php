<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Photo Gallery</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="style/form.css">
    <link rel="stylesheet" href="style/userPage.css">
    <?php include "templates/iconInsertion.html"; ?>
</head>

<body>
    <?php
    session_start();
    require "components/db.php";
    include "templates/navbarMain.html";
    include "components/data.php";
    $user = $_SESSION['username'];
    ?>

    <div class="container-fluid userForm">
        <div class="profileForm rounded">
            <?php
            // $sql = "SELECT * FROM users";
            // $result = mysqli_query($con, $sql);
            // while ($row = mysqli_fetch_assoc($result)) {
            //     $id = $row['id'];
            //     $sqlImg = "SELECT * FROM profile_img WHERE user_id= '$id'";
            //     $resultImg = mysqli_query($con, $sqlImg);
            echo "<img class='profilePic rounded' src='assets/default user.png'/>";
            //     while ($rowImg = mysqli_fetch_assoc($resultImg)) {


            //         if ($rowImg['status'] == 1) {
            //             echo "<img class='profilePic rounded' src='assets/profile" . $id . ".png'/>";
            //         } else {
            //             echo "<img class='profilePic rounded' src='assets/default user.png'/>";
            //         }
            //     }
            // }

            if ($isAdmin) {
            ?>
                <h3 class="admin">
                    <?php
                    echo "&#160;~ Admin ~";
                    ?>
                </h3>
            <?php
            }
            ?>
            <?php
            $images = mysqli_query($con, "SELECT count(username) FROM users JOIN images ON users.id = id_user WHERE username = '$user' ORDER BY date_added DESC");
            while ($row = mysqli_fetch_assoc($images)) {
                $rows = $row['count(username)'];
            }
            ?>

            <h2> <?php echo "<br>" . $user; ?> </h2>
            <h3>You have uploaded <?php echo $rows ?> pictures!</h3>
            <h4>Account created on <span class="createdData"></span>.</h4>

            <script>
                let data = `<?php echo $created_at; ?>`;
                data = data.slice(0, 10);
                let element = document.querySelector('.createdData');
                element.innerText = data;
            </script>
            <br>
            <form action="components/changePass.php">
                <button class="btn btn-outline-primary btnProfile">Change Password</button>
            </form>
            <br>
            <form action="components/changeUserPicture.php">
                <button class="btn btn-outline-primary btnProfile">Change Profile Picture</button>
            </form>
            <br>
            <?php
            if ($isAdmin) {
            ?>
                <div>
                    <a href="manageUsers.php" class="adminBtn btn btn-outline-primary btnProfile">
                        <w class="fix">Manage Users</w>
                    </a>
                    <a class="adminBtn btn btn-outline-primary btnProfile">
                        <w class="fix">Manage Posts</w>
                    </a>
                </div>
                <br>
            <?php
            }
            ?>
            <div action="" class="show">
                <button class="btn btn-outline-primary btnProfile showBtn">Show Gallery</button>
            </div>
            <div action="" class="hide">
                <button class="btn btn-outline-primary btnProfile hideBtn">Hide Gallery</button>
            </div>
            <br>
            <form action="components/logout.php">
                <button class="btn btn-outline-danger btnProfile">Log Out</button>
            </form>
            <br>
        </div>
    </div>
    <div style="z-index: -5;" class="overlay"></div>
    <?php
    include "templates/profileBack.html";
    $query = "SELECT * FROM images WHERE id_user = '$id_user' ORDER BY date_added DESC";
    $result = mysqli_query($con, $query);
    if ($result) {
    ?>
        <div class="profilePhotoBoxDiv">
            <?php
            function myIf($commSql)
            {
                $string_ = '';
                if ($commSql) {
                    while ($row_ = mysqli_fetch_array($commSql)) {
                        $comm = $row_['comm'];
                        $user_ = $row_['username'];
                        $id_comm = $row_['id'];
                        $string_ .= "<div class='fix_ p-2'><p class='m-0 commTag text-break text-start'>" . $user_ . ": " . $comm .  "</p>
                        <button data-idcomm='$id_comm' class='btn_ trashBtn pl-2 btn btn-outline-danger'><i class='fas fa-trash'></i></button></div>";
                    }
                }
                return $string_;
            }
            while ($row = mysqli_fetch_assoc($result)) {
                $desc = $row['description'];
                $upload_at = $row['date_added'];
                $img_id = $row['id'];

                $query = "SELECT * FROM comm WHERE post_id = '$img_id'";
                $commSql = mysqli_query($con, $query);
                echo "<div class='profilePhotoBox rounded'>
                <div class='top'>
                <p style='font-weight: 400; margin: 10px;' class='h6'>" . $desc . "</p>
                <p style='font-weight: 400; margin: 10px;' class='h6'>" . $upload_at . "</p>
                </div>
                <img class='img img-fluid mx-auto d-block' src='uploads/" . $row['filename'] . "'alt='Image not found!' >
                <div class='postBtn'>
                <button class='m-1 mt-3 mb-3 btn btn-outline-primary'>Edit</button>
                <button class='del m-1 mt-3 mb-3 btn btn-outline-danger'>Delete</button>
                <button data-imgid='$img_id' id='yn' class='yes m-1 mt-3 mb-3 btn btn-outline-danger'>Yes</button>
                <button id='yn' class='no m-1 mt-3 mb-3 btn btn-outline-success'>No</button>
                </div>
                <div class='comment w-100'>" . myIf($commSql) . "</div>
                </div>";
            }
            ?>
        </div>
    <?php
    }
    require "components/delete.php";
    include "templates/footer.html";
    include "templates/bootstrapInsertion.html";
    ?>
</body>

</html>
<script>
    $('.showBtn').click(function() {
        $('.hide').show();
        $('.show').hide();
        $('.profilePhotoBoxDiv').show();
    })
    $('.hideBtn').click(function() {
        $('.hide').hide();
        $('.show').show();
        $('.profilePhotoBoxDiv').hide();
    })
    $('.yes').click(function() {
        $.post('components/delete.php', {
            img_id: $(this)[0].dataset.imgid
        }).done(function() {
            window.location.reload();
        })
    })
    $('.del').click(function() {
        $(this).hide();
        $(this).prev().hide();
        $(this).next().show();
        $(this).next().next().show();
    })
    $('.no').click(function() {
        $(this).hide();
        $(this).prev().hide();
        $(this).prev().prev().show();
        $(this).prev().prev().prev().show();
    })
    $('.btn_').click(function() {
        // console.log($(this)[0].dataset.idcomm);
        $.post('components/deleteComm.php', {
            id_comm: $(this)[0].dataset.idcomm
        }).done(function() {
            window.location.reload();
        })
    })
</script>