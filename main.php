<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Photo Gallery</title>
    <?php include "templates/iconInsertion.html"; ?>
    <link rel="stylesheet" href="style/mainPage.css">
</head>

<body>
    <?php
    session_start();
    require "components/db.php";
    include "templates/navbarMain.html";
    include "components/auth_session.php";
    include "components/upload.php";
    include "components/data.php";
    ?>

    <div class="header rounded">
        <div class="userType">
            <h2 class="userName">Hey
                <?php
                echo $username . "!";
                ?>
            </h2>
        </div>
        <form class="" method="post" enctype="multipart/form-data">
            <div> <input type="text" class="titlePost" name="titleDesc" placeholder="Description" autocomplete="off"></div>
            <div>
                <input accept="image/* " type="file" name="uploadFile" value="" class="inputFile" />
                <label class="inputLabel btn btn-outline-primary" for="uploadFile">Choose a photo</label>
                <button class="btn btn-outline-success" type="submit" name="upload">Upload</button>
            </div>
        </form>
    </div>

    <?php
    $image_details  = mysqli_query($con, "SELECT * FROM users JOIN images ON users.id = id_user ORDER BY date_added DESC");
    if ($image_details) {
        function myIf($commSql)
        {
            $string_ = '';
            if ($commSql) {
                while ($row_ = mysqli_fetch_array($commSql)) {
                    $comm = $row_['comm'];
                    $user_ = $row_['username'];
                    $string_ .= "<p class='commTag text-break'>" . $user_ . ": " . $comm . "</p>";
                }
            }
            return $string_;
        }
        while ($row = mysqli_fetch_array($image_details)) {
            $desc = $row['description'];
            $upload_at = $row['date_added'];
            $userJoinName = $row['username'];
            $img_id = $row['id'];

            $query = "SELECT * FROM comm WHERE post_id = '$img_id'";
            $commSql = mysqli_query($con, $query);

            echo "<div class='photoBox rounded'>
            <div class='top'>
            <p id='weightTop' class='h3'>" . $userJoinName . " <w id='weightTop' class='h5 leftP'>" . $upload_at . "</w></p>
            <p class='h6 photoDescription'>" . $desc . "</p>
            </div>
            <img style='box-sizing: content-box;' class='img img-fluid mx-auto d-block' src='uploads/" . $row['filename'] . "'alt='Image not found!' >
            " . myIf($commSql) . "
            <div class='comment'><a class='comm link-primary'>Leave a comment!</a>
            <div class='commForm'>
                <textarea style='resize: none;'' style class='form-control txt' name='comm' type='text' placeholder='Leave a comment!'></textarea>
                <button data-imgid='$img_id' class='btn btn-outline-success txtSubmit' type='submit'>Comment</button>
                <a class='cancel btn btn-outline-danger'>Cancel</a>
            </div>
            </div>
            </div>";
        }
    }
    ?>
    <div class="overlay"></div>
</body>

<?php
include "templates/footer.html";
include "templates/bootstrapInsertion.html";
?>

</html>

<script>
    $('.comm').click(function() {
        $(this).next().show();
        $(this).hide();
    })
    $('.cancel').click(function() {
        $(this).parent().hide();
        $(this).parent().prev().show();
    })
    $('.txtSubmit').click(function() {
        console.log($(this)[0].dataset.imgid);
        let comm_ = $(this).prev().val();
        $.post('components/comm.php', {
            comm_: comm_,
            img_id: $(this)[0].dataset.imgid
        }).done(function() {
            window.location.reload();
        })
    })

    let input = document.querySelector('.inputFile');
    let label = document.querySelector('.inputLabel');
    label.addEventListener('click', () => {
        input.click();
    })
</script>